package psp.u1.ejer2.padrepardt2;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.ProcessBuilder.Redirect;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Hello world!
 *
 */
public class App {

	public static void main(String[] args) throws InterruptedException {

		String randomPath = "java -jar jar/part2-1.0.0.jar ";

		try {

			ArrayList<String> numeros = new ArrayList<String>();

			FileWriter fichero = new FileWriter("randoms.txt");
			PrintWriter pw = new PrintWriter(fichero);

			// pb.redirectOutput(Redirect.INHERIT);

			// pb.inheritIO();
			String linea = "";

			ProcessBuilder pb = new ProcessBuilder(randomPath.split(" "));
			//Para poder escribir en el scanner del hijo Input es el Output del hijo
			pb.redirectInput(Redirect.INHERIT);
			Process random10 = pb.start();
			do {

				InputStream inStream = random10.getInputStream();
				InputStreamReader inStreamReader = new InputStreamReader(inStream);
				BufferedReader bufReader = new BufferedReader(inStreamReader);

				linea = bufReader.readLine();
				System.out.println(linea);
				numeros.add(linea);

			} while (linea != null);

			System.out.println("Esto es lo que ha devuelto");

			for (String string : numeros) {
				System.out.println(string);
				pw.println(string);
			}

			pw.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
